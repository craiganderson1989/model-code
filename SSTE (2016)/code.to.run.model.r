#### This file illustrates the usage of the spatial clustering
#### model by applying it to simulated disease data


###Source the libraries required
library(CARBayes)
library(clusterSim)
library(mclust)
library(MCMCpack)
library(shapefiles)
library(spdep)




###Load the R functions which run the model
source("euclidean2.r")
source("mult.produce.W.r")
source("W_update2.R")



###Set the size of the scalar which controls the size of differences 
###between simulated clusters.  In the paper, the cases C=0,0.5,1 are explored.
scale <- 1



###Load the clustering template outlined in Figure 1 of the main paper
template <- read.csv("clusters.csv")
cluster.nums <- template[,4]
clustdata <- template[,3:4]
rownames(clustdata) <- template[,1]
n <- nrow(template)


###Compute piecewise means for the simulated data
mean.phi <- template[,3]*scale



###Read in neighbourhood matrix
shp <- read.shp(shp.name="england and wales local authority.shp")
dbf <- read.dbf(dbf.name="england and wales local authority.dbf")

data.combined <- combine.data.shapefile(clustdata, shp, dbf)
W.nb <- poly2nb(data.combined, row.names = rownames(clustdata))
W.list <- nb2listw(W.nb, style="B")
W <- nb2mat(W.nb, style="B")


###Create a spatially smooth covariance matrix
Wstar <- -W
diag(Wstar) <- as.numeric(apply(W, 1, sum))
Q <- 0.99 * Wstar + 0.01 * diag(rep(1,n))
variance.phi <- 0.005 * solve(Q)



#Set expected disease case values
E.real <- rep(100, n)
E.prior <- matrix(100, ncol=3,nrow=n)



### Simulate a "real" data set and a "prior" data set.
### The "prior" random effects = "real" random effects + uniform noise.
phi.real <- mvrnorm(n=1, mu=mean.phi, Sigma=variance.phi)
phi.prior1 <- phi.real + runif(n=n, min=-0.05, max=0.05)
phi.prior2 <- phi.real + runif(n=n, min=-0.1, max=0.1)
phi.prior3 <- phi.real + runif(n=n, min=-0.15, max=0.15)
  
mean.real <- E.real * exp(phi.real)
mean.prior1 <- E.prior[,1] * exp(phi.prior1)
mean.prior2 <- E.prior[,2] * exp(phi.prior2)
mean.prior3 <- E.prior[,3] * exp(phi.prior3)
  
Y.real <- rpois(n=n, lambda=mean.real)
Y.prior1 <- rpois(n=n, lambda=mean.prior1)
Y.prior2 <- rpois(n=n, lambda=mean.prior2)
Y.prior3 <- rpois(n=n, lambda=mean.prior3)
  
  

###Run the spatial clustering algorithm (stage 1 of the model)
Y.prior<- cbind(Y.prior1, Y.prior2, Y.prior3)
prior.SIR <- Y.prior / E.prior
log.prior.SIR <- log(prior.SIR)
  
produce.W <- mult.produce.W.list(log.prior.SIR, W)
  
  
  
###Set initial MCMC parameters
data <- as.matrix(Y.real)
W <- W
W.list <- produce.W$W.list
b.start <- 0.1
phi.start <- rep(0.1, n+1)
tau2.start <- 0.1
W.start.num <- 10
theta.start <- 1
var.theta <- 0.01
block.size.b <- 5
block.size.phi <- 10 
n.rep=10000
b.prior.var=10
prop.var.b=0.01
tau2.prior.shape=0.001 
tau2.prior.scale=0.001
E.i=as.matrix(E.real)
  


###Run MCMC function for spatial clustering model
W.update <- update.W(data=data, W=W, W.list=W.list, b.start=b.start, phi.start=phi.start,
                       tau2.start=tau2.start, W.start.num=W.start.num, theta.start=theta.start,
                       var.theta=var.theta, block.size.b=block.size.b, 
                       block.size.phi=block.size.phi, n.rep=10000,b.prior.var=10, prop.var.b=0.01,
                       tau2.prior.shape=0.001, tau2.prior.scale=0.001,E.i=E.i)
  
b <- median(W.update$b.store[5001:10000])
phi <- apply(W.update$phi.store[5001:10000,],2,median)[1:n]
risk <- exp(b + phi)



###Assess the quality of the results

###Number of clusters
freq <- table(W.update$W.store)
NUM.CLUST <- as.numeric(names(freq)[which.max(freq)])


###Rand Index
num.mode <- n + 1 - NUM.CLUST
RAND <- comparing.Partitions(cluster.nums,produce.W$cluster.store[num.mode,],type="rand")



##RMSE

RMSE <- sqrt(mean((exp(phi.real)-risk)^2))

  