mult.produce.W.list <- function(logrisk, W.contiguity){
  
  ###Read in Libraries and Functions###
  library(MASS)
  library(mclust)
  library(MCMCpack)
  source("euclidean2.R")
  
  n <- nrow(logrisk)
  
  
  clust.select <- euclid.cluster.func(logrisk,W.contiguity)
  
  W.list <- clust.select$W.list
  
  #new.W.list <- W.list
  new.W.list <- vector("list", n)
   
  
  for(j in 1:n){
    k=n+1-j
    w.star <- rep(NA, n)
    for(r in 1:n){
      w.star[r] <- 1-sum(sum(W.list[[k]][r,]==W.contiguity[r,])==n) 
    }
    W.mat <- matrix(W.list[[k]], nrow=nrow(W.list[[k]]))
    new.W.mat <- rbind(cbind(W.mat,w.star),c(w.star,0))
    new.W.list[[j]] <- Matrix(new.W.mat)
  }
  
  cluster.store <- clust.select$cluster.store

  new.list <- list(W.list=new.W.list,cluster.store=cluster.store)
  return(new.list)
  
}