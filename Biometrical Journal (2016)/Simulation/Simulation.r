######################################
####Code to run a single simulation###
######################################

###Source the libraries required
library(MCMCpack)
library(Rcpp)
library(truncdist)
library(mclust)
library(clusterSim)


###Load the R function and Rcpp function to run the model
source('../Model Code/clust_mcmc.r')
Rcpp::sourceCpp('../Model Code/clustfunctions.cpp')


###Set the size of the scalars which controls the size of differences 
###between simulated clusters.  In the paper, both values are set equal
###to a single value Z (Z=0,0.5,1 are explored), but this code allows
###them to be varied separately.
int.scale <- 0
slope.scale <- 0


###Set the maximum number of clusters allowed by the model.  In the paper
###both values are set equal to 5, while the supplementary material outlines
###results when both are set equal to 3,4,6 and 7.  This code also allows
###the number of intercept clusters (num.C) and slope clusters (num.D) to
###be varied separately
num.C <- 5
num.D <- 5



###Load the clustering template outlined in Figure 1 of the main paper
bothsimnums <- read.csv("cluster_template.csv")


###Compute piecewise means for intercept and slope
mean.phi <- bothsimnums$Intercept * int.scale
mean.delta <- bothsimnums$Slope * slope.scale


###Create a spatially smooth variance matrix
W <- dget(file="../Shapefiles/neighbourhood_matrix.txt")
Q <- -W
diag(Q) <- as.numeric(apply(W, 1, sum)) + 0.001
sigma <- 0.001*solve(Q)


###Set priors for hyperparameters
theta.C <- 5
theta.D <- 5
rho <- 0.99
tau.start <- 0.1
lambda <- 0.99
sigma.start <- 0.1
normal.prior.var <- 10
gamma.prior.scale <- 0.001
gamma.prior.shape <- 0.001



###Simulate data based on the simulation template.
n.time <- 10
alpha <- 0
phi <- mvrnorm(n=1, mu=mean.phi, Sigma=sigma)
beta <- 0
delta <- mvrnorm(n=1, mu=mean.delta, Sigma=sigma)
Y <- matrix(nrow=271,ncol=n.time)
E <- matrix(100,nrow=271,ncol=n.time)
risk.true <- matrix(n.time,nrow=271,ncol=n.time)
time.old <- seq(0.1,1,length.out=n.time)
time <- (time.old - mean(time.old))
for(t in 1:n.time){
  for(i in 1:271){
    risk <- exp(alpha + phi[i] + (beta + delta[i])*time[t])
     risk.true[i,t] <- risk
    Y[i,t] <- rpois(n=1,lambda=E[i,t]*risk)
  }
}



###Run the spatio-temporal clustering model based on the simulated data
mod <- MCMCfunc(Y=Y, E=E, W=W, n.rep=10000, num.C=num.C, num.D=num.D, time=time,
                rho=rho, tau=tau.start, theta.C=theta.C, lambda=lambda, 
                sigma=sigma.start, theta.D=theta.D, 
                normal.prior.var=normal.prior.var,
                gamma.prior.shape=gamma.prior.shape,
                gamma.prior.scale=gamma.prior.scale)



###Obtain parameter estimates
alpha.med <- apply(as.matrix(mod$alpha.store[5000:10000,]), 2, median)
beta.med <- apply(as.matrix(mod$beta.store[5000:10000,]), 2, median)
phi.med <- apply(mod$phi.store[5000:10000, ], 2, median)
delta.med <- apply(mod$delta.store[5000:10000, ], 2, median)
theta.C.med <- median(mod$theta.C.store[5000:10000])
theta.D.med <- median(mod$theta.D.store[5000:10000])
C.med <- apply(mod$C.store[5000:10000,],2,median)
D.med <- apply(mod$D.store[5000:10000,],2,median)
alpha.est <- alpha.med[C.med]
beta.est <- beta.med[D.med]



########################################
####Assess the quality of the results###
########################################


###Rand Index
true.clust <- mean.phi + 0.1*mean.delta
fitted.clust <- C.med + 0.1*D.med
RAND <- comparing.Partitions(true.clust,fitted.clust,type="rand")


###Number of clusters
NUM.CLUST <- length(table(fitted.clust))


##RMSE
risk.est.clust <- matrix(nrow=271,ncol=n.time)
for(t in 1:n.time){
  for(i in 1:271){
    risk.est.clust[i,t] <- exp(alpha.est[i] + phi.med[i] + (beta.est[i] + delta.med[i])*time[t])
  }
}

RMSE <- sqrt(mean((risk.true-risk.est.clust)^2))