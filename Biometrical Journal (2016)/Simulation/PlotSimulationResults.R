#########################
####Code for Figure 1 ###
#########################

### The R plotting scheme did not allow me to include hatchmarks for slope clusters
### Instead, the data from "cluster_template.csv" was passed into QGIS to obtain the
### plots found in the paper.



#########################
####Code for Figure 2 ###
#########################

### Read in the data from the simulation studies
none5 <- read.csv("none5.csv")
low5 <- read.csv("bothlow5.csv")
high5 <- read.csv("bothhigh5.csv")


### Set up a 3x1 plotting grid
par(mfrow=c(3,1), oma=c(0,0,2,0))

### Boxplot of Number of Clusters
numclust.all5 <- cbind(none5$PClust, none5$BClust, low5$PClust, low5$BClust, 
                       high5$PClust, high5$BClust)
boxplot(numclust.all5, ylim=c(1,20), xaxt="n", ylab="Number of clusters", main="Number of Clusters")
axis(side=1, at=1:6, label=rep(c("Proposed", "Bernardinelli"),3))
lines(c(2.5, 2.5), c(-3,90))
lines(c(4.5, 4.5), c(-3,90))
lines(c(2.5, 10),c(9, 9),  lty=2)
lines(c(-3, 2.5),c(1, 1),  lty=2)
text(x=c(1.5, 3.5, 5.5), y=rep(19.5,3), c("Z=0", "Z=0.5", "Z=1"))


### Boxplot of Rand Index
rand.all5 <- cbind(none5$PRand, none5$BRand, low5$PRand, low5$BRand, 
                   high5$PRand, high5$BRand)
boxplot(rand.all5, ylim=c(0.1,1.1), xaxt="n", ylab="Rand Index", main="Rand Index")
axis(side=1, at=1:6, label=rep(c("Proposed", "Bernardinelli"),3))
lines(c(2.5, 2.5), c(-3,90))
lines(c(4.5, 4.5), c(-3,90))
text(x=c(1.5, 3.5, 5.5), y=rep(1.08,3), c("Z=0", "Z=0.5", "Z=1"))


### Boxplot of RMSE
rmse.all5 <- cbind(none5$PRMSE, none5$BRMSE, low5$PRMSE, low5$BRMSE, 
                   high5$PRMSE, high5$BRMSE)
boxplot(rmse.all5, ylim=c(0,0.25), xaxt="n", ylab="RMSE", main="RMSE for the estimated risk surface")
axis(side=1, at=1:6, label=rep(c("Proposed", "Bernardinelli"),3))
lines(c(2.5, 2.5), c(-3,90))
lines(c(4.5, 4.5), c(-3,90))
text(x=c(1.5, 3.5, 5.5), y=rep(0.24,3), c("Z=0", "Z=0.5", "Z=1"))