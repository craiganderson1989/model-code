########################################################################################
### INTRODUCTION
########################################################################################

This folder contains the code and data for the following manuscript:

TITLE: Spatial clustering of average risks and risk trends in Bayesian disease mapping

AUTHORS: Craig Anderson, Duncan Lee, and Nema Dean


The code was written by Craig Anderson, with some assistance from both co-authors.
Please contact craig.anderson@uts.edu.au to report any bugs or to ask any questions
you have about the code.

This code was written and tested under the following configuration.

R version 3.2.1 (2015-06-18)
Platform: x86_64-apple-darwin13.4.0 (64-bit)
Running under: OS X 10.10.5 (Yosemite)


########################################################################################
### ABOUT THE CODE
########################################################################################

This code can be used to reproduce the results displayed in the manuscript.

There are 5 subfolders:

@@@@@@@@@@@@@@@@@@@
@@@ Application @@@
@@@@@@@@@@@@@@@@@@@

This folder contains the code relevant to the case study of respiratory disease in 
Glasgow outlined in Section 5 of the manuscript.

The script "RunGlasgowData.R" contains the code to repeat the analysis outlined in the
paper.  This script is self-contained, and sources relevant files (data and model) from
the other subfolders. 


@@@@@@@@@@@@
@@@ Data @@@
@@@@@@@@@@@@

This folder contains the data relating to respiratory disease in Glasgow.

The data was obtained from the Scottish Neighbourhood Statistics website 
(http://www.sns.gov.uk).

The file "respiratory_admissions.csv" contains counts of the yearly numbers of 
respiratory admissions to hospital between 2002 and 2011 for each areal unit.

The file "expected_counts.csv" contains the expected numbers of hospital admissions for
each area unit based on standardised age and sex specific disease rates.

Note that for both files, the column "IG" contains unique area identifiers for the 
271 areal units within the Greater Glasgow and Clyde Health board.


@@@@@@@@@@@@@@@@@@
@@@ Model Code @@@
@@@@@@@@@@@@@@@@@@

This folder contains the code to actually fit the model outlined in the paper.

The R function "clust_mcmc.r" contains code to carry out MCMC sampling for the model
outlined in Section 3.2 of the manuscript.

The function "clustfunctions.cpp" contains a number of Rcpp functions which speed up
the computation of the MCMC.


@@@@@@@@@@@@@@@@@@
@@@ Shapefiles @@@
@@@@@@@@@@@@@@@@@@

This folder contains the geographical shapefiles for Scotland.  These shapefiles allow
the code to identify the location of each areal unit for the purpose of modelling and
plotting.

The file "neighbourhood_matrix.txt" contains the 271x271 neighbourhood matrix for
the Glasgow region.  The neighbourhood matrix identifies which regions are
geographically adjacent for the purposes of autocorrelation.  The neighbourhood matrix
is further defined in Section 2 of the manuscript.

The files "Scotland_IG.dbf", "Scotland_IG.shp" and "Scotland_IG.shx" contain the
geographical co-ordinates of each of the areal units of Scotland, and allow each unit
to be plotted on a map.


@@@@@@@@@@@@@@@@@@
@@@ Simulation @@@
@@@@@@@@@@@@@@@@@@

This folder contains the code relevant to the simulation study outlined in Section 4
of the manuscript.

The files "bothhigh5.csv", "bothlow5.csv" and "none5.csv" contain the results from the
simulations carried out under each of the 3 data generation approaches discussed in
Section 4.  These files contain the results from 200 simulated datasets with 
Z=1, Z=0.5 and Z=0 respectively.  In each file, the columns "PClust", "PRand" and 
"PRMSE" refer to the number of clusters, Rand Index and RMSE obtained under the
proposed model, while "BClust", "BRand" and "BRMSE" refer to the Bernardinelli model.

The file "cluster_template.csv" contains details of the clusters used for the 
simulation study.

The script "PlotSimulationResults.R" contains the code necessary to plot the results
of the simulation study.

The script "Simulation.r" contains code to allow the user to run a single simulation.



########################################################################################
### REPRODUCING FIGURES FROM THE MANUSCRIPT
########################################################################################

The data and code provided here allow the user to reproduce each of the 4 figures from
the manuscript.  Most of these figures were generated in R, but two of them used
QGIS, which is a geographic information system application.  These QGIS plots were
constructed manually using the GUI, and therefore code for these plots does not
exist and cannot be provided.  However, instructions are provided which would allow the
user to construct these plots in QGIS should they wish to do so.

The figures in the manuscript can be reproduced as follows:

Figure 1 - constructed using QGIS.  See "PlotSimulationResults.R" for more information.

Figure 2 - run the script "PlotSimulationResults.R".

Figure 3 - run the script "RunGlasgowData.R".  The code for both the top and bottom
panels of the figure is clearly identified within this script.

Figure 4 - run the script "RunGlasgowData.R".  The plot in the top panel was constructed
using QGIS, but code for an alternative plot is outlined in this script.  The plot in
the bottom panel was generated in R, and the code for this is clearly identified.

